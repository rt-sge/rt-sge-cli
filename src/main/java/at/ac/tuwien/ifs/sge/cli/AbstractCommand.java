package at.ac.tuwien.ifs.sge.cli;

import java.io.File;
import java.util.List;

public abstract class AbstractCommand {

  public abstract SgeCommand getSge();

  public abstract boolean[] getQuiet();

  public abstract boolean[] getVerbose();

  public abstract int getNumberOfPlayers();

  public abstract String getBoard();

  public abstract List<File> getFiles();

  public abstract List<File> getDirectories();

  public abstract List<String> getAgentConfiguration();

  public abstract List<String> getArguments();

  protected abstract void setNumberOfPlayers(int numberOfPlayers);

  protected abstract void setBoard(String board);

  protected void loadLogLevel() {
    if (getVerbose().length != 0 || getQuiet().length != 0) {
      getSge().log
          .setLogLevel(getQuiet().length - getVerbose().length);
    }
  }

  protected void loadArguments() {
    getSge()
        .determineArguments(getArguments(), getFiles(), getDirectories(), getAgentConfiguration());
    getSge().processDirectories(getFiles(), getDirectories());
  }

  protected void loadFiles() {
    getSge().log.tra_("Files: ");

    for (File file : getFiles()) {
      getSge().log._tra_(file.getPath() + " ");
    }

    getSge().log._trace();

    getSge().loadFiles(getFiles(), getBoard());
    getSge().log.debug("Successfully loaded all files.");
  }


  protected void loadFillAgentList(int p) {
    getSge().fillAgentList(getAgentConfiguration(), p);
  }

  protected void loadBoard() {
    setBoard(getSge().interpretBoardString(getBoard()));
    if (getBoard() == null) {
      getSge().log.debug("No initial board given.");
    } else {
      getSge().log
          .debug("Initial board: " + getBoard().split("\n")[0] + (getBoard().contains("\n") ? "..."
              : ""));
    }
  }

  protected void printAgentConfiguration() {
    getSge().log.deb_("Configuration: ");
    for (String s : getAgentConfiguration()) {
      getSge().log._deb_(s + " ");
    }
    getSge().log._debug();
  }

  protected void loadCommon() {
    loadLogLevel();
    loadArguments();
    loadFiles();
    //loadBoard();
  }
}
