package at.ac.tuwien.ifs.sge.cli;

import at.ac.tuwien.ifs.sge.core.agent.Agent;
import at.ac.tuwien.ifs.sge.core.agent.MemorySpecifications;
import at.ac.tuwien.ifs.sge.core.engine.factory.MatchFactory;
import at.ac.tuwien.ifs.sge.core.engine.factory.RealTimeMatchFactory;
import at.ac.tuwien.ifs.sge.core.engine.factory.TurnBasedMatchFactory;
import at.ac.tuwien.ifs.sge.core.engine.game.tournament.TournamentMode;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

import at.ac.tuwien.ifs.sge.core.game.GameConfiguration;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;
import picocli.CommandLine.Parameters;
import picocli.CommandLine.ParentCommand;

@Command(name = "tournament", aliases = {"t"}, description = "Let agents play in a tournament.")
public class TournamentCommand extends AbstractCommand implements Runnable {

  @Option(names = {"-c",
      "--computation-time"}, defaultValue = "60", description = "Amount of computational time given for each action.")
  long computationTime = 60;

  @Option(names = {"-l",
          "--time-limit"}, defaultValue = "300", description = "Amount of time a real time match can take in total. ")
  long timeLimit = 5 * 60;

  @Option(names = {"-u",
      "--time-unit"}, defaultValue = "SECONDS", description = "Time unit in which -c or -l is. Valid values: ${COMPLETION-CANDIDATES}")
  TimeUnit timeUnit = TimeUnit.SECONDS;

  @ParentCommand
  private SgeCommand sge;

  @Option(names = {"-h", "--help"}, usageHelp = true, description = "Prints this message.")
  private boolean helpRequested = false;

  @Option(names = {"-q",
      "--quiet"}, description = "Found once: Log only warnings. Twice: only errors. Thrice: no output")
  private boolean[] quiet = new boolean[0];

  @Option(names = {"-v",
      "--verbose"}, description = "Found once: Log debug information. Twice: with trace information")
  private boolean[] verbose = new boolean[0];

  @Option(names = {"-p",
      "--number-of-players"}, arity = "1", paramLabel = "N", description = "Number of players. By default the minimum the tournament supports.")
  private int numberOfPlayers = (-1);

  @Option(names = {"-b",
      "--board"}, arity = "1", paramLabel = "BOARD", description = "Optional: Initial board position. Can either be a file or a string.")
  private String board = null;

  @Option(names = {"-f",
      "--file"}, arity = "1..*", paramLabel = "FILE", description = "File(s) of game and agents.")
  private List<File> files = new ArrayList<>();

  @Option(names = {"-d",
      "--directory"}, arity = "1..*", paramLabel = "DIRECTORY", description = "Directory(s) of game and agents. Note that all subdirectories will be considered.")
  private List<File> directories = new ArrayList<>();

  @Option(names = {"-a",
      "--agent"}, arity = "1..*", paramLabel = "AGENT", description = "Configuration of agents.")
  private List<String> agentConfiguration = new ArrayList<>();

  @Option(names = {"-r", "-s", "--shuffle"}, description = "Shuffle configuration of agents.")
  private boolean shuffle = false;

  @Option(names = {"-m",
      "--mode"}, arity = "1", paramLabel = "MODE", description = "Tournament Mode. Valid values: ${COMPLETION-CANDIDATES}")
  private TournamentMode tournamentMode = TournamentMode.ROUND_ROBIN;

  @Option(names = {
      "--max-actions"}, arity = "1", paramLabel = "N",
      description = "Maximum number of actions. Game is aborted after the Nth action. Per default (the maximum) 2^31-2.")
  private int maxActions = Integer.MAX_VALUE - 1;

  @Option(names = {"--min-heap"}, arity = "1", paramLabel = "BYTES",
          description = "Minimum heap size per agent. Per default 256M. Valid values: [x, xk, xK, xm, xM, xg, xG] where x is a number. Only multiple of 1024 Bytes are allowed.")
  private String minHeapSize = Agent.DEFAULT_MIN_HEAP_SIZE;

  @Option(names = {"--max-heap"}, arity = "1", paramLabel = "BYTES",
          description = "Maximum heap size per agent. Per default 256M. Valid values: [x, xk, xK, xm, xM, xg, xG] where x is a number. Only multiple of 1024 Bytes are allowed.")
  private String maxHeapSize = Agent.DEFAULT_MAX_HEAP_SIZE;

  @Option(names = {"--stack-size"}, arity = "1", paramLabel = "BYTES",
          description = "Thread stack size per agent. Per default 1024k. Valid values: [x, xk, xK, xm, xM, xg, xG] where x is a number. Only multiple of 1024 Bytes are allowed.")
  private String threadStackSize = Agent.DEFAULT_THREAD_STACK_SIZE;

  @Parameters(index = "0", arity = "0..*", description = {
      "Not explicitly specified files or configuration of agents."})
  private List<String> arguments = new ArrayList<>();


  @Override
  public void run() {
    loadCommon();

    int min = Math
        .max(sge.gameFactory.getMinimumNumberOfPlayers(), tournamentMode.getMinimumPerRound());
    int max = Math
        .min(sge.gameFactory.getMaximumNumberOfPlayers(), tournamentMode.getMaximumPerRound());

    if (numberOfPlayers < 0) {
      numberOfPlayers = min;
    } else if (!(min <= numberOfPlayers && numberOfPlayers <= max)) {
      sge.log.error("Tournament cannot be played with this number of players.");
      throw new IllegalArgumentException("Illegal player number.");
    }

    if (agentConfiguration.size() < min) {
      if (!agentConfiguration.isEmpty()) {
        sge.log.warn("Not enough agents, filling up with others.");
      }
      sge.fillAgentList(agentConfiguration);
    }

    if (shuffle) {
      Collections.shuffle(agentConfiguration);
    }

    printAgentConfiguration();

    var memorySpecifications = new MemorySpecifications(minHeapSize, maxHeapSize, threadStackSize);
    var agentList = sge
        .createAgentListFromConfiguration(agentConfiguration, memorySpecifications);

    MatchFactory matchFactory;
    if (sge.gameFactory.isRealtime()) {
      matchFactory = new RealTimeMatchFactory(
              sge.gameFactory,
              GameConfiguration.fromString(board),
              numberOfPlayers,
              sge.log,
              sge.pool,
              timeUnit.toMillis(timeLimit),
              false
      );
    } else {
      matchFactory = new TurnBasedMatchFactory(
              sge.gameFactory,
              GameConfiguration.fromString(board),
              numberOfPlayers,
              sge.log,
              sge.pool,
              false,
              computationTime,
              timeUnit,
              maxActions
      );
    }

    var tournament = tournamentMode
            .getTournament(
                    matchFactory,
                    agentList,
                    numberOfPlayers,
                    sge.log
            );

    try {
      tournament.call();
    } catch (Exception e) {
      sge.log.warn("Could not finish tournament.");
      sge.log.printStackTrace(e);
    }

    System.out.println("\n".concat(tournament.toTextRepresentation()).concat("\n"));
  }

  @Override
  public SgeCommand getSge() {
    return sge;
  }

  @Override
  public boolean[] getQuiet() {
    return quiet;
  }

  @Override
  public boolean[] getVerbose() {
    return verbose;
  }

  @Override
  public int getNumberOfPlayers() {
    return numberOfPlayers;
  }

  @Override
  public String getBoard() {
    return board;
  }

  @Override
  public List<File> getFiles() {
    return files;
  }

  @Override
  public List<File> getDirectories() {
    return directories;
  }

  @Override
  public List<String> getAgentConfiguration() {
    return agentConfiguration;
  }

  @Override
  public List<String> getArguments() {
    return arguments;
  }

  @Override
  protected void setNumberOfPlayers(int numberOfPlayers) {
    this.numberOfPlayers = numberOfPlayers;
  }

  @Override
  protected void setBoard(String board) {
    this.board = board;
  }
}
